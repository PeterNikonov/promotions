package tests

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"promotions/internal/loyalty"
	"promotions/internal/process"
	"runtime"
	"testing"
	"time"
)

func TestParallelPerformance(t *testing.T) {
	matchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	notMatchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/not_match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	offerJson, err := ioutil.ReadFile("offer_resolver/payload/offer.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	var mr = process.Order{}
	var nmr = process.Order{}
	var offer = loyalty.Offer{}

	json.Unmarshal(matchRequestJson, &mr)
	json.Unmarshal(notMatchRequestJson, &nmr)
	json.Unmarshal(offerJson, &offer)

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(mr.Fields), func(i, j int) { mr.Fields[i], mr.Fields[j] = mr.Fields[j], mr.Fields[i] })

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(nmr.Fields), func(i, j int) { nmr.Fields[i], nmr.Fields[j] = nmr.Fields[j], nmr.Fields[i] })

	var orders []process.Order

	orders = append(orders, mr)
	orders = append(orders, nmr)

	var offers []loyalty.Offer

	for i := 0; i < 100000; i++ {
		offers = append(offers, offer)
	}

	fmt.Println("Num cpu:")
	fmt.Println(runtime.GOMAXPROCS(runtime.NumCPU()))

	parOffer := process.ParOffer{}
	start := time.Now()
	for _, r := range orders {
		result := parOffer.Start(offers, r)
		fmt.Println(len(result))
	}
	elapsedTime := time.Since(start)
	fmt.Println("Total Time For Parallel Execution: " + elapsedTime.String())
}
