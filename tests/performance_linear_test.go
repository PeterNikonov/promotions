package tests

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"promotions/internal/loyalty"
	"promotions/internal/process"
	"testing"
	"time"
)

func TestPreLinearPerformance(t *testing.T) {
	matchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	notMatchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/not_match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	offerJson, err := ioutil.ReadFile("offer_resolver/payload/offer.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	var mr = process.Order{}
	var nmr = process.Order{}
	var offer = loyalty.Offer{}

	json.Unmarshal(matchRequestJson, &mr)
	json.Unmarshal(notMatchRequestJson, &nmr)
	json.Unmarshal(offerJson, &offer)

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(mr.Fields), func(i, j int) { mr.Fields[i], mr.Fields[j] = mr.Fields[j], mr.Fields[i] })

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(nmr.Fields), func(i, j int) { nmr.Fields[i], nmr.Fields[j] = nmr.Fields[j], nmr.Fields[i] })

	var orders []process.Order

	orders = append(orders, mr)
	orders = append(orders, nmr)

	var offers []loyalty.Offer

	for i := 0; i < 100000; i++ {
		offers = append(offers, offer)
	}

	start1 := time.Now()
	for _, r := range orders {
		result := process.HandleSimple(r, offers)
		fmt.Println(len(result))
	}
	elapsedTime1 := time.Since(start1)
	fmt.Println("Total Time For Linear Execution: " + elapsedTime1.String())
}

func TestLinearPerformance(t *testing.T) {
	matchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	notMatchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/not_match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	offerJson, err := ioutil.ReadFile("offer_resolver/payload/offer.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	var mr = process.Order{}
	var nmr = process.Order{}
	var offer = loyalty.Offer{}

	json.Unmarshal(matchRequestJson, &mr)
	json.Unmarshal(notMatchRequestJson, &nmr)
	json.Unmarshal(offerJson, &offer)

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(mr.Fields), func(i, j int) { mr.Fields[i], mr.Fields[j] = mr.Fields[j], mr.Fields[i] })

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(nmr.Fields), func(i, j int) { nmr.Fields[i], nmr.Fields[j] = nmr.Fields[j], nmr.Fields[i] })

	var orders []process.Order

	orders = append(orders, mr)
	orders = append(orders, nmr)

	var offers []loyalty.Offer

	for i := 0; i < 100000; i++ {
		offers = append(offers, offer)
	}

	start1 := time.Now()
	for _, r := range orders {
		result := process.HandleSimple(r, offers)
		fmt.Println(len(result))
	}
	elapsedTime1 := time.Since(start1)
	fmt.Println("Total Time For Linear Execution: " + elapsedTime1.String())
}

func TestLinearOptimizePerformance(t *testing.T) {
	matchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	notMatchRequestJson, err := ioutil.ReadFile("offer_resolver/payload/not_match_request.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	offerJson, err := ioutil.ReadFile("offer_resolver/payload/offer.json")
	if err != nil {
		fmt.Errorf(err.Error())
	}

	var mr = process.Order{}
	var nmr = process.Order{}

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(mr.Fields), func(i, j int) { mr.Fields[i], mr.Fields[j] = mr.Fields[j], mr.Fields[i] })

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(nmr.Fields), func(i, j int) { nmr.Fields[i], nmr.Fields[j] = nmr.Fields[j], nmr.Fields[i] })

	var offer = loyalty.Offer{}

	json.Unmarshal(matchRequestJson, &mr)
	json.Unmarshal(notMatchRequestJson, &nmr)
	json.Unmarshal(offerJson, &offer)

	var orders []process.Order

	orders = append(orders, mr)
	orders = append(orders, nmr)

	var offers []loyalty.Offer

	for i := 0; i < 100000; i++ {
		offers = append(offers, offer)
	}

	start1 := time.Now()
	for _, r := range orders {
		result := process.HandleSimpleOptimize(&r, offers)
		fmt.Println(len(result))
	}
	elapsedTime1 := time.Since(start1)
	fmt.Println("Total Time For Linear Optimize Execution: " + elapsedTime1.String())
}
