package tests

import (
	"encoding/json"
	"io/ioutil"
	"promotions/internal/loyalty"
	"promotions/internal/process"
	"testing"
)

// TestProcess тест проверяет корректность работы алгоритма сопоставления предложения и заказа
func TestProcess(t *testing.T) {
	offerJson, err := ioutil.ReadFile("./payload/offer.json")
	if err != nil {
		t.Error(err.Error())
	}
	var offer = loyalty.Offer{}
	json.Unmarshal(offerJson, &offer)

	var offers []loyalty.Offer
	offers = append(offers, offer)

	// заказ удовлетворяющий условию предложения
	matchOrderJson, err := ioutil.ReadFile("./payload/match_order.json")
	if err != nil {
		t.Error(err.Error())
	}

	var matchOrder = process.Order{}
	json.Unmarshal(matchOrderJson, &matchOrder)

	resultB := process.Handle(matchOrder, offers)
	if resultB == nil {
		t.Error("Match order has not offer!")
	}

	// заказ неудовлетворяющий условию предложения
	notMatchOrderJson, err := ioutil.ReadFile("./payload/not_match_order.json")
	if err != nil {
		t.Error(err.Error())
	}

	var notMatchOrder = process.Order{}
	json.Unmarshal(notMatchOrderJson, &notMatchOrder)

	resultA := process.Handle(notMatchOrder, offers)
	if resultA != nil {
		t.Error("Not match order has offer!")
	}
}

// TestProcess тест проверяет корректность работы алгоритма сопоставления предложения и заказа
func TestProcessParallel(t *testing.T) {
	offerJson, err := ioutil.ReadFile("./payload/offer.json")
	if err != nil {
		t.Error(err.Error())
	}
	var offer = loyalty.Offer{}
	json.Unmarshal(offerJson, &offer)

	var offers []loyalty.Offer
	offers = append(offers, offer)

	// заказ удовлетворяющий условию предложения
	matchOrderJson, err := ioutil.ReadFile("./payload/match_order.json")
	if err != nil {
		t.Error(err.Error())
	}

	var matchOrder = process.Order{}
	json.Unmarshal(matchOrderJson, &matchOrder)

	parOffer := process.ParOffer{}
	resultB := parOffer.Start(offers, matchOrder)
	if resultB == nil {
		t.Error("Match order has not offer!")
	}

	// заказ неудовлетворяющий условию предложения
	notMatchOrderJson, err := ioutil.ReadFile("./payload/not_match_order.json")
	if err != nil {
		t.Error(err.Error())
	}

	var notMatchOrder = process.Order{}
	json.Unmarshal(notMatchOrderJson, &notMatchOrder)

	resultA := parOffer.Start(offers, notMatchOrder)
	if resultA != nil {
		t.Error("Not match order has offer!")
	}
}

func TestSimpleOptimizeProcess(t *testing.T) {
	offerJson, err := ioutil.ReadFile("./payload/offer.json")
	if err != nil {
		t.Error(err.Error())
	}
	var offer = loyalty.Offer{}
	json.Unmarshal(offerJson, &offer)

	var offers []loyalty.Offer
	offers = append(offers, offer)

	// заказ удовлетворяющий условию предложения
	matchOrderJson, err := ioutil.ReadFile("./payload/match_order.json")
	if err != nil {
		t.Error(err.Error())
	}

	var matchOrder = process.Order{}
	json.Unmarshal(matchOrderJson, &matchOrder)

	resultB := process.HandleSimpleOptimize(&matchOrder, offers)
	if resultB == nil {
		t.Error("Match order has not offer!")
	}

	// заказ неудовлетворяющий условию предложения
	notMatchOrderJson, err := ioutil.ReadFile("./payload/not_match_order.json")
	if err != nil {
		t.Error(err.Error())
	}

	var notMatchOrder = process.Order{}
	json.Unmarshal(notMatchOrderJson, &notMatchOrder)

	resultA := process.HandleSimpleOptimize(&notMatchOrder, offers)
	if resultA != nil {
		t.Error("Not match order has offer!")
	}
}
