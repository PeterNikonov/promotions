#docker
сборка образа

```
docker build -t promotions:latest .
``` 

публикация образа

```
docker push nikonovp/promotions:latest
```

#minikube
запуск 
```
minikube start
```

k8s деплой и сервис
```
kubectl apply -f deployment.yml

minikube service list

minikube service  promotions-service

```
