FROM golang:1.13 as builder
WORKDIR /opt/promotions
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -o promotions ./cmd/promotions

FROM alpine:3.10
EXPOSE 8080
WORKDIR /opt/promotions
COPY --from=builder /opt/promotions/promotions .
CMD ["/opt/promotions/promotions"]