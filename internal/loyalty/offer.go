package loyalty

// поле предложения - название и значение
type Field struct {
	// название поля
	Name string `json:"name"`
	// заданное значение
	Value string `json:"value"`
	// паттерн сравнения, прим. %request_value% == %offer_value%
	Pattern string `json:"pattern"`
}

// Offer предложение
type Offer struct {
	Fields []Field `json:"fields"`
}

// AddField добавляет условие в предложение
func (o *Offer) AddField(f Field) {
	if f.Value != "" {
		o.Fields = append(o.Fields, f)
	}
}

// GetFields возвращает все поля предложения
func (o *Offer) GetFields() []Field {
	return o.Fields
}
