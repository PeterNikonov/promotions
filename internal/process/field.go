package process

import (
	"promotions/internal/comparator"
	"promotions/internal/loyalty"
	"strings"
)

// FieldTask параллельно выполняющаяся задача - заказ проверяем на совпадение с условиями промо-предложения
type FieldTask struct {
	value   string
	field   loyalty.Field
	results chan<- loyalty.Field
}

// Run проверить соответствие заказа конкретному предложению
func (t FieldTask) Run() {
	assertion := strings.NewReplacer(ORDER_VALUE, t.value, OFFER_VALUE, t.field.Value).Replace(t.field.Pattern)
	c := comparator.NewComparator()
	condition, err := c.Parse(assertion)
	if err != nil {
		return
	}
	if c.Compare(condition) {
		t.results <- t.field
	}
}

// параллельное сравнение полей в одном предложении
type ParField struct{}

func NewParField() *ParField {
	return &ParField{}
}

func (r *ParField) Start(offer loyalty.Offer, order Order) bool {
	tasks := make(chan FieldTask, workers)
	results := make(chan loyalty.Field, len(offer.GetFields()))
	done := make(chan struct{}, workers)

	go r.setTasks(tasks, offer, order, results)

	// запустить параллельную обработку задач
	for i := 0; i < workers; i++ {
		go r.runTasks(done, tasks)
	}

	go r.awaitCompletion(done, results)

	// если верны все утверждения
	return len(r.processResults(results)) == len(offer.GetFields())
}

func (r *ParField) setTasks(tasks chan<- FieldTask, offer loyalty.Offer, order Order, results chan<- loyalty.Field) {
	for _, field := range offer.GetFields() {
		tasks <- FieldTask{
			field:   field,
			value:   order.GetValue(field.Name),
			results: results,
		}
	}
	close(tasks)
}

func (r *ParField) runTasks(done chan<- struct{}, tasks <-chan FieldTask) {
	for t := range tasks {
		t.Run()
	}
	done <- struct{}{}
}

func (r *ParField) awaitCompletion(done <-chan struct{}, results chan loyalty.Field) {
	for i := 0; i < workers; i++ {
		<-done
	}
	close(results)
}

func (r *ParField) processResults(results chan loyalty.Field) []loyalty.Field {
	var rl []loyalty.Field
	for result := range results {
		rl = append(rl, result)
	}

	return rl
}
