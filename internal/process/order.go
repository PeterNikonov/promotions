package process

// Order заказ, содержит поля со значениями
type Order struct {
	Fields []*Field `json:"fields"`
	//представление для ускорения работы
	mappedValues map[string]string
}

// Field поле заказа со значением
type Field struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// выполняет наполнение маппинга
func (o *Order) performMapped() {
	m := make(map[string]string)
	for _, f := range o.Fields {
		m[f.Name] = f.Value
	}
	o.mappedValues = m
}

// получает значение указанного своейства
func (o *Order) GetValue(fieldName string) string {
	// если маппинг не подготовлен - подготовим, делаем один раз - перфоманс))
	if len(o.mappedValues) == 0 {
		o.performMapped()
	}
	// вернем значение, если оно существует
	if val, ok := o.mappedValues[fieldName]; !ok {
		return ""
	} else {
		return val
	}
}
