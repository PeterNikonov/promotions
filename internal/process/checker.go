package process

import (
	"promotions/internal/comparator"
	"promotions/internal/loyalty"
	"strings"
)

const (
	ORDER_VALUE = "%order_value%"
	OFFER_VALUE = "%offer_value%"
)

// Checker проверяющий условия
type Checker struct {
	comparator *comparator.Comparator
	offer      loyalty.Offer
}

func NewChecker(comparator *comparator.Comparator, offer loyalty.Offer) *Checker {
	return &Checker{comparator: comparator, offer: offer}
}

// IsMatched определяет совпадение конкретного признака с указанным выражением
func (r *Checker) IsMatched(name string, value string) bool {
	for _, f := range r.offer.GetFields() {
		if f.Name == name {
			assert := strings.NewReplacer(ORDER_VALUE, value, OFFER_VALUE, f.Value).Replace(f.Pattern)
			condition, err := r.comparator.Parse(assert)
			if err != nil {
				return false
			}

			return r.comparator.Compare(condition)
		}
	}

	return false
}
