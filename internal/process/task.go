package process

import (
	"promotions/internal/loyalty"
)

// Task параллельно выполняющаяся задача - заказ проверяем на совпадение с условиями промо-предложения
type Task struct {
	// offer - условия промо-предложения
	offer loyalty.Offer
	// order - содержание заказа
	order *Order
	// checker - проверяющий
	checker *Checker
	// results найденные предложения
	results chan<- loyalty.Offer
}

// Run проверить соответствие заказа конкретному предложению
func (t Task) Run() {
	match := 0
	for _, f := range t.order.Fields {
		if t.checker.IsMatched(f.Name, f.Value) == true {
			match++
		}
	}

	// если все условия предложения удовлетворены, считаем, что предложение найдено.
	if len(t.offer.GetFields()) == match {
		t.results <- t.offer
	}
}

// Task параллельно выполняющаяся задача - заказ проверяем на совпадение с условиями промо-предложения
type PTask struct {
	// offer - условия промо-предложения
	offer loyalty.Offer
	// order - содержание заказа
	order *Order
	// checker - проверяющий
	checker *Checker
	// results найденные предложения
	results chan<- loyalty.Offer
}

// Run проверить соответствие заказа конкретному предложению
func (t PTask) Run() {
	match := 0
	for _, f := range t.order.Fields {
		if t.checker.IsMatched(f.Name, f.Value) == true {
			match++
		}
	}

	// если все условия предложения удовлетворены, считаем, что предложение найдено.
	if len(t.offer.GetFields()) == match {
		t.results <- t.offer
	}
}
