package process

import (
	"promotions/internal/comparator"
	"promotions/internal/loyalty"
	"runtime"
)

var workers = runtime.GOMAXPROCS(runtime.NumCPU())
// Handle общий смысл обработчика - распаралелить поиск совпадений промо-предложений для заказа,
// т.е. один заказ может удовлетворять
// нескольким предложениям, задача быстро найти все совпадения
func Handle(order Order, offers []loyalty.Offer) []loyalty.Offer {
	// канал задач, буфер по количеству доступных ядер
	tasks := make(chan Task, workers)
	// канал найденных предложений, буфер по количеству известных предложений
	results := make(chan loyalty.Offer, len(offers))
	// канал из которого узнаем, что задача отработала
	done := make(chan struct{}, workers)

	go setTasks(tasks, offers, &order, results)

	// запустить параллельную обработку задач
	for i := 0; i < workers; i++ {
		go runTasks(done, tasks)
	}

	go awaitCompletion(done, results)

	return processResults(results)
}

// setTasks инициализировать постановку задач
func setTasks(tasks chan<- Task, offers []loyalty.Offer, order *Order, results chan<- loyalty.Offer) {
	for _, o := range offers {
		tasks <- Task{
			offer:   o,
			order:   order,
			checker: NewChecker(comparator.NewComparator(), o),
			results: results,
		}
	}
	close(tasks)
}

// runTasks запустить выполнение задач
func runTasks(done chan<- struct{}, tasks <-chan Task) {
	for t := range tasks {
		t.Run()
	}
	done <- struct{}{}
}

// awaitCompletion завершить выполнение
func awaitCompletion(done <-chan struct{}, results chan loyalty.Offer) {
	for i := 0; i < workers; i++ {
		<-done
	}
	close(results)
}

// processResults
func processResults(result <-chan loyalty.Offer) []loyalty.Offer {
	var r []loyalty.Offer
	for result := range result {
		r = append(r, result)
	}

	return r
}
