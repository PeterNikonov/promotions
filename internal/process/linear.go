package process

import (
	"promotions/internal/comparator"
	"promotions/internal/loyalty"
	"strings"
)

func HandleSimple(request Order, offers []loyalty.Offer) []loyalty.Offer {
	var result []loyalty.Offer
	comparator := comparator.NewComparator()

	for _, o := range offers {
		checker := NewChecker(comparator, o)
		matchCount := 0
		for _, f := range request.Fields {
			if checker.IsMatched(f.Name, f.Value) == true {
				matchCount++
			}
		}
		if len(o.GetFields()) == matchCount {
			result = append(result, o)
		}
	}

	return result
}

// HandleSimpleOptimize по тестам это самый быстрый алгоритм
func HandleSimpleOptimize(order *Order, offers []loyalty.Offer) []loyalty.Offer {
	var result []loyalty.Offer
	comparator := comparator.NewComparator()
	for _, offer := range offers {
		matchCount := 0
		for _, field := range offer.GetFields() {
			value := order.GetValue(field.Name)
			assertion := strings.NewReplacer(ORDER_VALUE, value, OFFER_VALUE, field.Value).Replace(field.Pattern)
			condition, err := comparator.Parse(assertion)
			if err != nil {
				break
			}
			if comparator.Compare(condition) {
				matchCount++
			}
		}
		if len(offer.GetFields()) == matchCount {
			result = append(result, offer)
		}
	}

	return result
}
