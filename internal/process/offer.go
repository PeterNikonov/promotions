package process

import (
	"promotions/internal/loyalty"
)

// параллельное сравнение предложений
type ParOffer struct{}

type OfferTask struct {
	// offer - условия промо-предложения
	offer loyalty.Offer
	// order - содержание заказа
	order Order
	// results найденные предложения
	results chan<- loyalty.Offer
}

func (t OfferTask) Run() {
	pf := NewParField()
	if pf.Start(t.offer, t.order) {
		t.results <- t.offer
	}
}

func (r *ParOffer) Start(offers []loyalty.Offer, order Order) []loyalty.Offer {
	tasks := make(chan OfferTask, workers)
	results := make(chan loyalty.Offer, len(offers))
	done := make(chan struct{}, workers)

	go r.setTasks(tasks, offers, order, results)

	// запустить параллельную обработку задач
	for i := 0; i < workers; i++ {
		go r.runTasks(done, tasks)
	}

	go r.awaitCompletion(done, results)

	return r.processResults(results)
}

func (r *ParOffer) setTasks(tasks chan<- OfferTask, offers []loyalty.Offer, order Order, results chan<- loyalty.Offer) {
	for _, offer := range offers {
		tasks <- OfferTask{
			offer:   offer,
			order:   order,
			results: results,
		}
	}
	close(tasks)
}

func (r *ParOffer) runTasks(done chan<- struct{}, tasks <-chan OfferTask) {
	for t := range tasks {
		t.Run()
	}
	done <- struct{}{}
}

func (r *ParOffer) awaitCompletion(done <-chan struct{}, results chan loyalty.Offer) {
	for i := 0; i < workers; i++ {
		<-done
	}
	close(results)
}

func (r *ParOffer) processResults(result <-chan loyalty.Offer) []loyalty.Offer {
	var resolved []loyalty.Offer
	for result := range result {
		resolved = append(resolved, result)
	}

	return resolved
}
