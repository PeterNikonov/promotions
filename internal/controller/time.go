package controller

import (
	"encoding/json"
	"net/http"
	"promotions/cmd/promotions/config"
	"time"
)

// TimeController контроллер запросов времени
type TimeController struct {
	config *config.Config
}

func NewTimeController(config *config.Config) *TimeController {
	return &TimeController{config: config}
}

// Time возвращает текущее время
func (s *TimeController) Time(w http.ResponseWriter, r *http.Request) {
	// json.NewEncoder(w).Encode(s.config.Enemies)
	json.NewEncoder(w).Encode(time.Now().String())
}
