package comparator

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const (
	equal            = " == "
	not_equal        = " != "
	greater          = " > "
	less             = " < "
	greater_or_equal = " >= "
	less_or_equal    = " <= "
	contains         = " contains "     // contains string
	has_prefix       = " has_prefix "   // value begins with another string
	has_suffix       = " has_suffix "   // value string has_suffix another string
	in               = " in "           // value is in a list
	not_in           = " not_in "       // value is not in a list
	match_regexp     = " match_regexp " // value matches a regular expression
)

// operators supported compare operators
var operators = []string{
	equal,
	not_equal,
	greater,
	less,
	greater_or_equal,
	less_or_equal,
	contains,
	has_prefix,
	has_suffix,
	in,
	not_in,
	match_regexp,
}

// Comparator comparing by pattern-expression
type Comparator struct {
}

// construct
func NewComparator() *Comparator {
	return &Comparator{}
}

// Condition native structure to comparison
type Condition struct {
	operator string
	left     string
	right    string
}

// parse parsing pattern-condition string
func (c *Comparator) Parse(pattern string) (*Condition, error) {
	unlist := func(x []string) (string, string) {
		return strings.TrimSpace(x[0]), strings.TrimSpace(x[1])
	}

	for _, o := range operators {
		x := strings.Split(pattern, o)
		if len(x) == 2 {
			left, right := unlist(x)

			return &Condition{
				operator: o,
				left:     strings.ToLower(left),
				right:    strings.ToLower(right),
			}, nil
		}
	}

	return nil, fmt.Errorf("usupported operator: %s", pattern)
}

// Compare native compare realization
func (c *Comparator) Compare(cond *Condition) bool {
	switch cond.operator {
	case equal:
		return strings.ToLower(cond.left) == strings.ToLower(cond.right)
	case not_equal:
		return strings.ToLower(cond.left) != strings.ToLower(cond.right)
	case greater:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l > r
	case less:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l < r
	case greater_or_equal:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l >= r
	case less_or_equal:
		l, _ := strconv.ParseInt(cond.left, 0, 32)
		r, _ := strconv.ParseInt(cond.right, 0, 32)
		return l <= r
	case contains:
		return strings.Contains(cond.left, cond.right)
	case has_prefix:
		return strings.HasPrefix(cond.left, cond.right)
	case has_suffix:
		return strings.HasSuffix(cond.left, cond.right)
	case in:
		slice := strings.Split(cond.right, ",")
		for _, s := range slice {
			if strings.TrimSpace(s) == cond.left {
				return true
			}
		}
		return false
	case not_in:
		slice := strings.Split(cond.right, ",")
		for _, s := range slice {
			if strings.TrimSpace(s) == cond.left {
				return false
			}
		}
		return true
	case match_regexp:
		regexp, _ := regexp.Compile(cond.right)
		return regexp.MatchString(cond.left)
	}

	return false
}
