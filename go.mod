module promotions

go 1.14

require (
	github.com/jessevdk/go-flags v1.4.0
	go.uber.org/zap v1.15.0
)
